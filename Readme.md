# DiKoala

DiKoala is tool for searching DICOM files based on DICOM header information. It was developed by Sebastian Hirsch at the Berlin Center for Advanced Neuroimaging @ Charité - Universitätsmedizin Berlin.

 DiKoala can operate in two different modes: **filter** and **collect**.

In **filter** mode, DiKoala expects one or several filter condition(s) based on DICOM header attributes, such as "StudyDate >= 2013-03-14". It will then recursively scan a DICOM folder and list all files which satisfy the condition. Alternatively, if the -d flag is used, it will only inspect the first DICOM file in each folder and report the results for each folder rather than each file. See section "Compound conditions" below for examples of how several conditions can be compounded using logical operators.

In **collect** mode, a DICOM attribute is specified rather than a condition. DiKoala then scans all files (or only the first file in each folder, if the -d flag ist used) in the DICOM folder for the value of the DICOM attribute and returns all found values, along with the number of occurrences of each value.

The format of the mandatory argument determines whether **filter** (a condition) or **collect** (a single DICOM header attribute name) mode will be used. See "Searching arbitrary DICOM attributes" below for a description of the condition format for **filter** mode.

Newer Siemens Scanners running on the XA software line (such as Lumina, Vida, ...) use a nested DICOM structure to represent multiple slices/images within a single DICOM file. To search the entire DICOM header structure, rather than just the first level, use the `--extended` (or `-x`) command line switch. Note that this can make scanning large directories much slower, since the entire DICOM file has to be read, rather than only the header up to the requested attribute(s).

**Note**: This document uses the nomenclature for DICOM headers laid out in [DICOM Nomenclature.md](./DICOM Nomenclature.md).

## Compound conditions
In **filter** mode, several conditions can be stated and connected by logical operators. For example,

    "(StudyDate >= 2021-10-01) and (StudyDate <= 2021-10-31)"

will scan for all files acquired in October 2021. "or" can be used instead of "and" if only one condition has to be fulfilled. Every condition has to be enclosed in parentheses (). An arbitrary number of conditions can be used.

### Precedence rules
The precedence of logical operators (*and*, *or*) within a condition is determined by the position within the condition string, and can be modified by using additional parentheses. The precedence increases from left to right within the condition string. For example,

    "(cond1) and (cond2) and (cond3)"

is parsed as

    "(cond1) and ((cond2) and (cond3))"

To change the order of precedence, use additional parentheses:

    "((cond1) and (cond2)) and (cond3)".

(*Note:* The above examples are somewhat nonsensical since a & (b & c) == (a & b) & c, but if combinations of *and* and *or* are used, precedence does matter.)

To display a syntax tree representing the order in which a condition string would be applied, use the `--show-condition-tree` (or short `-t`) option:

```
> dikoala.sh -t "(cond1 == a) and (cond2 > b) or (cond3 contains c)"       

ConditionNode with condition "and"
    ConditionNode with condition "=="
        HeaderNode: cond1
        ConstantNode: a
    ConditionNode with condition "or"
        ConditionNode with condition ">"
            HeaderNode: cond2
            ConstantNode: b
        ConditionNode with condition "contains"
            HeaderNode: cond3
            ConstantNode: c
```
Compare this to the parenthesized version:
```
> dikoala.sh -t "((cond1 == a) and (cond2 > b)) or (cond3 contains c)"                                                                            
ConditionNode with condition "or"
    ConditionNode with condition "and"
        ConditionNode with condition "=="
            HeaderNode: cond1
            ConstantNode: a
        ConditionNode with condition ">"
            HeaderNode: cond2
            ConstantNode: b
    ConditionNode with condition "contains"
        HeaderNode: cond3
        ConstantNode: c
```
Note how the top-level ConditionNode has changed from *and* to *or*.

## Examples
(**Note:** the package provides shell scripts/batch files to start DiKoala on Linux/Unix/MacOS and Windows. The following examples use the Linux bash script. In a Windows environment, use dikoala.bat instead of dikoala.sh)


### Search by acquisition date (**filter** mode)
Search all files acquired on Pi Day in 2021 in DICOMFolder and its subfolders:

    dikoala.sh -p [DICOMFolder] "AcquisitionDate == 2021-03-14" 
If DICOMFolder is omitted, the current working directory is scanned recursively. In addition to "==", the following operators are supported:
    
    ">", "<", ">=", "<=", "!="

### List all acquisition dates (**collect mode**)
    dikoala.sh "AcquisitionDate"
This will produce a list with all encountered acquisition dates, and a count of how often each date was found. Since this command operates in file mode, each single DICOM file with contribute a date (i.e. a scan with 256 DICOM images will produce 256 counts for the same date). Therefore, it might make more sense to use dirmode (a.k.a. folder mode) instead (see below), so that each folder only produces one count.

### Folder mode
Instead of scanning every DICOM file (which can take very long), only inspect the first DICOM file in each folder and report the folders that meet the search condition:

    dikoala.sh -p [DICOMFolder] --dirmode "AcquisitionDate == 2021-03-14" 
The shortcut -d can be used as an alternative to --dirmode

### Searching arbitrary DICOM attributes
The general search syntax
"DICOMKeyword <operator> value"
Value is automatically converted to the right format based on the Value Representation of the DICOM attribute (see section "Value Representation" below) See [DICOM Nomenclature.md](./DICOM Nomenclature.md) for an explanation of the nomenclature used regarding Attributes, Keywords etc.

For all representations, the following operators are available:

    "==", ">", "<", ">=", "<=", "!="

For data types that represent a string, `contains` (and its negated form `!contains`) can also be used to check if the left hand side contains the right hand side at any position:

Example: ```PatientName contains ohn``` - will match all patients with name John, and anyone with *ohn* in their name.

**Caveat:** `contains` performs a case-*in*sensitive search.

By default, only the first level of the DICOM header is parsed. Newer Siemens scanner, running the XA software line, use nested headers (where fields can contain entire header structures). To search through all levels, use the `--extended` (or `-x`) command line switch. Note that this can slow down the scanning process.

### Value Representation
The DICOM standard defines different "Value Representations" to store different types of data in an Attribute's Data Element. The following table presents an overview of the existing representations. The column **Format** gives an example for how to enter data for use in a comparison. The column **Julia Representation** lists the data types into which the data is internally converted. Note that not all value representations are currently supported. Unimplemented value representations will either return a String (whether that is meaningful or not), or conversion might cause an exception.

| Value Representation | Meaning | Example Tag | Format | Julia Representation|
|---|---|---|---|---|
| AE | Application Entity | SourceApplicationEntityTitle | *not implemented*|None|
| AS | Age String | PatientAge | "PatientAge == 034Y" (as of now, will not be converted to a number) |String|
| CS | Code String | ImageType | "ImageType contains --P--" (search for phase images) | String (parts are joined with -- as the delimiter)
| DA | Date | StudyDate | "StudyDate > 2021-03-14" (accepted format is yyyy-mm-dd or yyyymmdd)|Dates.Date|
| DS | Decimal String | FlipAngle | "FlipAngle > 2.5" |Float|
| DT | DateTime | AcquisitionDateTime | Accepted format: "yyyy-dd-mm HH:MM:SS" or "yyyymmdd HH:MM:SS", e.g. "AcquisitionDateTime > 2020-07-14 14:58:00" |Dates.DateTime|
| FL | Floating Point Single | B1rms (not default) | "B1rms > 0.1" | Float |
| FD | Floating Point Double | AcquisitionDuration | AcquisitionDuration > 10 | Float |
| IS | Integer String |EchoTrainLength | "EchoTrainLength >= 64" |Int|
| LO | Long String | Manufacturer | "Manufacturer == SIEMENS"|String|
| LT | Long Text | ? | ? |String|
| OB | Other Byte String | FileMetaInformationVersion | *not implemented* |None|
| OF | Other Float String | ? | *not implemented*|None|
| OW | Other Word String | PixelData | *not implemented* |None|
| PN | Person Name | PatientName | "PatientName == Unicorn" (note: The DICOM standard defines a format with 5 parts of the name separated by backslashes, but this is not implemented yet.)|String|
| SH | Short String |StationName | "StationName == MYSCANNER" |String|
| SL | Signed Long | ? | *not implemented*|None|
| SQ | Sequence of Items | MREchoSequence (not standard) | Fields of this type contain a subheader structure. They can be parsed using the `--extended` command line option. |None|
| SS | Signed Short | ? | *not implemented* |None|
| ST | Short text | InstitutionAddress | "InstitutionAddress contains Berlin" |String|
| TM | Time | StudyTime | "StudyTime > 12:00:00" (matches all images acquired after noon on any day)|Dates.Time|
| UI | Unique Identifier | SOPClassUID | "SOPClassUID ==  1.2.840.10008.5.1.4.1.1.4" |String|
| UL | Unsigned Long | InStackPositionNumber (no standard) | "InStackPositionNumber == 4" | Int |
| US | Unsigned Short | Rows | "Rows == 128" |UInt|
| UT | Unlimited Text |? | *not implemented*|None|

## Installation
- Install Julia from [https://julialang.org/downloads/](https://julialang.org/downloads/).
- Clone the git repository: `git clone git@gitlab.com:sebastiancervus/dicomfilter.git` or download an archive file from [https://gitlab.com/sebastiancervus/dikoala](https://gitlab.com/sebastiancervus/dikoala) and unpack it to a folder of your choice.
  + Use the *main* branch for stable versions that have been tested.
  + The *dev* branch contains commits that might not work or have not been tested properly.
- Open a command window (Windows) or a terminal (Linux, MacOS).
- `cd` to the cloned folder
- Download and install dependencies: `julia --project=@. -e "using Pkg; Pkg.instantiate()"`
- Use `dikoala.sh` (Linux, Unix, MacOS) or `dikoala.bat (Windows) from a shell/command window.
