#=
ConditionParser:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-10-19
- This file is licensed under the 3-clause BSD license. See License.md for the full license text.
- Contact: sebastian.hirsch@bccn-berlin.de
=#

"""
This file provides methods for parsing a condition string provided as an input argument and produces
a BinaryExpressionTree from it.
"""

#include("BinaryExpressionTree.jl")

"""
    remove_surrounding_parentheses(s::String)::String

If s is enclosed by one or more pair(s) of parentheses, remove them and return the "naked" string.

If the number of opening and closing parentheses is not equal, an ErrorException is thrown.

# Examples
```jldoctest
julia> remove_surrounding_parentheses("(((abcd)))")
"abcd"

julia> remove_surrounding_parentheses("(abc)(def)")
"(abc)(def)"

julia> remove_surrounding_parentheses("((abc)(def))")
"(abc)(def)"
```
For example, for s="(((abcd)))", the function returns "abcd". For s="(abc)(def)", the output
is identical to the input, since there is no pair of parentheses enclosing the entire string.
For s="((abc)(def))", the returned value is "(abc)(def)".


"""
function remove_surrounding_parentheses(s::String)::String

    ss = String(strip(s))

    # check that the number of opening and closing parentheses is equal:
    n_open = length(findall(x->x=='(', ss))
    n_close = length(findall(x->x==')', ss))

    if n_open != n_close
        throw(ErrorException("Error: the number of opening and closing parentheses is not matched in $(ss)."))
    end

    if ss[1] != '('  # no opening parenthesis found -> nothing to remove
        return ss
    end

    # opening parenthesis found
    open_count = 0
    for i in 1:length(ss)
        if s[i] == '('
            open_count += 1
        elseif s[i] == ')'
            open_count -= 1
            if open_count == 0 && i != length(ss)
                # in that case, there is a parenthesis that closes the opening one, but that is not at the end of the string
                return ss
            end
        end
    end

    # if we get here, there is at least one pair of enclosing parentheses to be removed. We remove the outermost pair and check if there is another enclosing pair.
    ret = remove_surrounding_parentheses(String(ss[2:end-1]))
    return ret
end

"""
    extract_subexpressions(s::String)::(String, Dict{String, String})

If s is composed of multiple subexpressions/subconditions, simplify s by replacing each
subexpression with a symbol and produce a Dict to resolve the symbol to the original expression.

# Examples:
If s="(StudyDate==2019-10-10) and (PatientName contains a)", then the expressions
to the left and right of "and" will be considered subexpressions.

```jldoctest
julia> extract_subexpressions("(StudyDate==2019-10-10) and (PatientName contains a)")
("_item1_ and _item2_", Dict("_item2_" => "PatientName contains a", "_item1_" => "StudyDate==2019-10-10"))
```
"""
function extract_subexpressions(s::String, item_idx::Int=1)

    start = 0
    open_count = 0
    items = Dict{String, String}()

    result = ""
    until = 1

    s = remove_surrounding_parentheses(s)

    first = findfirst("(", s)
    if first === nothing  # no opening parenthesis found
        return s, items
    else
        start = first.start
        result = s[1:start-1]
    end


    for i=start:length(s)
        if open_count == 0
            start = i
        end
        if s[i] == '('
            open_count += 1
        elseif s[i] == ')'
            open_count -= 1
            if open_count == 0
                subexpr = s[start:i]
                item_str = "_item$(item_idx)_"

                subexpr = remove_surrounding_parentheses(subexpr)

                sub, d = extract_subexpressions(subexpr, item_idx+1)
                items[item_str] = sub
                if isempty(d)
                    item_idx += 1
                else
                    item_idx += length(d) + 1
                    items = merge(items, d)
                end

                result *= s[until+1:start-1]*item_str
                start = i+1
                #item_idx += 1
                until = i


            end
        end
    end

    start = max(start, 1)  # start could still be 0 if no opening parenthesis was found
    result *= s[start:end]
    return result, items
end

function expand_expression(s::String, dict::Dict{String,String})::String

    item_pattern = r"_item\d_"
    m = eachmatch(item_pattern, s)  # non-recursively capture all matches

    for x in m
        s = replace(s, x.match => dict[x.match])
    end

    return s
end

function create_symbolic_node(s::String, dict::Dict{String, String}, parent_node::Union{SymbolicNode, Nothing}=nothing, side::SIDE=SIDE_LEFT, ignore_case::Bool=false)::SymbolicNode

    operators = Dict{String, Function}(
        # map string reprensations of functions to actual binary functions
        ">" => >,
        "<" => <,
        ">=" => >=,
        "==" => ==,
        ">=" => >=,
        "<=" => <=,
        "!=" => !=,
        "contains" => (x,y) -> occursin(lowercase(y), lowercase(x)),  # the order of arguments for occursin is opposed to the order for "contains"
        "!contains" => (x,y) -> !occursin(lowercase(y), lowercase(x)),
        "and" => (x,y) -> x && y,
        "or" => (x,y) -> x || y
    )

    # sort operators by length from longest to shortest, to make sure that ">" does not match ">="
    ops = sort([x for x in keys(operators)], by=x->length(x), rev=true)

    operators_str = join(ops, "|")

    pattern = Regex("(\\S.+?)\\s*($operators_str)\\s*(\\S.*)")

    m = match(pattern, s)
    if m === nothing  # we found a leaf node that encodes either a DICOM Attribute or a constant value, or an _itemX_ string that requires further expansion.
        if occursin(r"^_item\d+_$", s)
            return create_symbolic_node(expand_expression(s, dict), dict, parent_node, side, ignore_case)
        else
            return SymbolicNode(s, nothing, parent_node, side)
        end
    end

    lhs = String(m[1])
    op_str = String(m[2])
    rhs = String(m[3])

    op = operators[op_str]
    # if the "negate" flag is set, we negate the condition generated above
    # op(x,y) = negate ? !tmp_op(x,y) : tmp_op(x,y)  # this doesn't work e.g. for ==, since a==b is the same as b == a
#    op(x,y) = tmp_op(x,y)  # <- insert negation here, if necessary

    left_node = create_symbolic_node(lhs, dict, parent_node, SIDE_LEFT, ignore_case)
    right_node = create_symbolic_node(rhs, dict, parent_node, SIDE_RIGHT, ignore_case)
    this_node = SymbolicNode(s, op, parent_node, side)

    left_node.parent = this_node
    this_node.left = left_node

    right_node.parent = this_node
    this_node.right = right_node

    return this_node
end

function symbolic_to_real_tree(n::SymbolicNode)::ExprTreeNode

    if n.left !== nothing && n.right !== nothing  # both children exist -> generate a condition node

        left = symbolic_to_real_tree(n.left)
        right = symbolic_to_real_tree(n.right)
        return ConditionNode(left, n.operation, right)

    elseif n.left === nothing && n.right === nothing
        # no children -> generate a HeaderNode (lhs) or a ConstantNode (rhs)
        if n.side == SIDE_LEFT
            str = String(strip(n.contents_str))
            return HeaderNode(str)
        else
            str = String(strip(n.contents_str))
            return ConstantNode(str)
        end

    else
        throw(ErrorException("Error: Node with just one child is not allowed!"))
    end
end

function make_condition_tree(query::String; ignore_case::Bool=false)::ExprTreeNode

    s, d = extract_subexpressions(query)
    sym_root = create_symbolic_node(s, d)
    symbolic_to_real_tree(sym_root)

end
