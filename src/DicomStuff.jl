#=
DicomValues:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-09-14
- This file is licensed under the 3-clause BSD license. See License.md for the full license text.
=#

#include("./Types.jl")

using Dates
using DICOM

"""
parse_field(val::Any, field_type::String)
Parse the value val of a DICOM Attribute to a specific type, indicated by field_type, which is
a two-letter code from the DICOM ValueRepresentation dictionary.
"""
function parse_field(val::Any, field_type::String)

    # See http://oftankonyv.reak.bme.hu/tiki-index.php?page=Value+representation+in+DICOM for an
    # overview of DICOM value representation

    int(x) = parse(Int64, x)  # shorthand for converion to int

    # these representations already come as the correct type and need no conversion
    return_unchanged = [  # these representations already come as the correct type and need no conversion
            "DS",  # Decimal String
            "IS",  # Integer String
            "US",  # Unsigned Short
            "FD",  # Float (double)
            "FL",  # Float (single)
            "UL"   # Unsigned long
    ]

    # these types are either strings and therefore do not need to be changed, or we do not yet
    # have a proper conversion for them and will return them as a string (which will likely cause
    # a problem somewhere.
    return_as_string = [
        "AE",  # Application entity
        "AS",  # Age String
        "AT",  # Attribute Tag
        "LO",  # Long (as in lengthy, not the number) String
        "LT",  # Long text
        "OB",  # Other Byte String
        "OF",  # Other Float String
        "OW",  # Other Word String
        "SH",  # Short string
        "SL",  # Signed long
        "SQ",  # Sequence of items
        "SS",  # Signed short
        "ST",  # Short text
        "UI",  # Unique Identifier
        "UN",  # Unknown
        "UT",  # Unlimited text
        ""     # This will happen if the field is not contained in the DICOM header (for whatever reason)
    ]

    if val === nothing || isempty(val)
        return nothing
    end

    ret = nothing

    if field_type in return_unchanged
        ret = val

    elseif field_type in return_as_string
        ret = String(val)  # might be Substring{String}, so we convert it here

    elseif field_type == "CS"  # Code String
        return(join(vec(val), "--"))

    elseif field_type == "DA"  # date: yyyymmdd
        date_str = val[1:8]  # if there is anything after the first eight characters (it should not
        # be, just to be safe), discard it
        ret = Date(date_str, "yyyymmdd")

    elseif field_type == "TM"  # time: HHMMss.fffff, but data type is SubString{String}, not String
        val = string(val)  # val might be a SubString
        # Dates.Time supports parsing of time strings only with millisecond resolution. Therefore,
        # if val has microsecond resolution, we null everything below 1 ms.
        val = remove_microseconds(val)
        ret = Time(val, "HHMMSS.s")

    elseif field_type == "DT"  # DateTime, format YYYYMMDDHHMMSS.FFFFFF&ZZXX  with
        #&ZZXX is an optional suffix for offset from Coordinated Universal Time (UTC), where & = “+” or “-”, and ZZ = Hours and XX = Minutes of offset
        # We ignore the fractional part and the timezone offset for now
        ret = DateTime(int(val[1:4]), int(val[5:6]), int(val[7:8]), int(val[9:10]), int(val[11:12]), int(val[13:14]))


    elseif field_type == "PN"  # Person name, five components, separated by backslash:
        # family name complex, given name complex, middle name, name prefix, name suffix
        return val

    else
        println("Unknown field type: $field_type")
    end

    return ret
end  # parse_field

"""
remove_microseconds(s::String)::String
Remove the microsecond part from a time string with the format
HHMMSS.s, e.g. "150957.123500"
Everything below millisecond resolution will be replaced with 0
"""

function remove_microseconds(s::String)::String
    position_of_point = findfirst(".", s)
    if position_of_point !== nothing
        p = position_of_point[1]
        number_of_decimals = length(s)-p
        if p > 3
            remove_digits = number_of_decimals-3
            tmp = chop(s, tail=remove_digits)  # remove anything below microsecond precision
            s = tmp * "0"^remove_digits
        end
    end
    s
end

"""
read_dicom_header(filename::String, max_group::UInt16=typemax(UInt16))::DICOM.DICOMData

Read the header of a DICOM file without the pixel data. This is achieved by reading the file only
up to group 0x7fe0-1. Group 0x7fe0 contains the actual image data and is excluded. This should be
significantly faster than reading the entire file when scanning through large collections of DICOM
data\n
Note: "Group" refers to the Group Number, i.e., the first element of the Data Element Tag (X, Y)
that denotes a DICOM Attribute.

Returns: a DICOMData structure
"""
function read_dicom_header(filename::String, max_group::UInt16=UInt16(0x7fe-1))::DICOM.DICOMData

    if max_group == 0x0
        max_group = typemax(UInt16)
    end
    # println("Reading header until " * string(max_group, base=16))
    dcm_parse(filename, max_group=max_group)
end

"""
find_in_header_extended!(header::DICOM.DICOMData, keyowrd, results::HeaderLookupResults=HeaderLookupResults())::HeaderLookupResults
Perform a (potentially recursive) scan for all DICOM data matching the keyword. This works for nested
header structures and values with multiplicity > 1.
"""
function find_in_header_extended!(header::DICOM.DICOMData, keyword, results::HeaderLookupResults=HeaderLookupResults())::HeaderLookupResults
    
    for key in keys(header)
        val = header[key]  # DICOMData does not support "for (key, val) in header", so we have to mimic it

        if key == keyword

            vr = get(header.vr, key, nothing)  # look up Value Representation

            push!(results, (val, vr))
        elseif isa(val, DICOM.DICOMData)
            find_in_header_extended!(val, keyword, results)
            
        elseif isa(val, Vector)
            for q in val
                if isa(q, DICOM.DICOMData)
                    find_in_header_extended!(q, keyword, results)
                end
            end
        end
    end
    return results
end

"""
header_lookup_extended(header::DICOM.DICOMData, keyword::String)::HeaderLookupResults
Look up an Attribute (via its Keyword) in a DICOMData object. This function also works for nested
DICOM headers with multiplicity > 1, but will most likely be much slower than the _standard version
of the same command.
"""
function header_lookup_extended(header::DICOM.DICOMData, keyword::String)::HeaderLookupResults
    
    try
        x = DICOM.fieldname_dict[Symbol(keyword)]
        results = find_in_header_extended!(header, x)  # results is a Vector of type (Val, VR) <-> (Any, String)
    
        for i in 1:length(results)
            r = results[i]
            if r[2] === nothing
                results[i] = (r[1], resolve_vr(header, x))
            end
        end
        return results
    
    catch err
        return HeaderLookupResults()
    end
end

"""
header_lookup_standard(header::DICOM.DICOMData, keyword::String)::HeaderLookupResults
Lookup a DICOM Attribute in the provided header structure. This function does not work for nested
header structures, use header_lookup_extended instead.
"""
function header_lookup_standard(header::DICOM.DICOMData, keyword::String)::HeaderLookupResults

    a = DICOM.fieldname_dict[Symbol(keyword)]

    val = nothing
    field_type = ""
    try
        field_type = header.vr[a]
        val = header[a]
    catch err
        if isa(err, KeyError)  # header does not contain the requested data
            return [(nothing, "")]
        end
    end

    # generate a 1-element vector with the result
    v = HeaderLookupResults()
    push!(v, (val, field_type))
    
    return v

end

"""
resolve_vr(header::DICOM.DICOMDate, group)::String
Lookup the Value Representation of the DICOM Attribute indicated by group (the Group Number in
hexadecimal representation) First we look in the Value Representation dictionary of the DICOM header,
if this does not succeed we check the generic dictionary provided by the DICOM package. As a last
resort, an empty string is returned.
"""
function resolve_vr(header::DICOM.DICOMData, group)::String
    if haskey(header, group)
        return header.vr[group]
    elseif haskey(DICOM.dcm_dict, group)
        return DICOM.dcm_dict[group][2]
    end

    return ""
end
