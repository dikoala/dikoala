#=
TreeVisualization:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-10-18
- This file is licensed under the 3-clause BSD license. See License.md for the full license text.
- Contact: sebastian.hirsch@bccn-berlin.de
=#

#include("BinaryExpressionTree.jl")


base_indent = "    "
function print_node(n::ConditionNode, indent=0)

    prefix = generate_prefix(indent)

    op_str = n.op_str

    if is_and(n.op)
        op_str = "and"
    elseif is_or(n.op)
        op_str = "or"
    elseif is_contains(n.op)
        op_str = "contains"
    end

    println("$(prefix)ConditionNode with condition \"$op_str\"")
    print_node(n.left, indent+1)
    print_node(n.right, indent+1)
end

function print_node(n::HeaderNode, indent=0)
    prefix = generate_prefix(indent)

    println("$(prefix)HeaderNode: $(n.keyword)")
end

function print_node(n::ConstantNode, indent=0)
    prefix = generate_prefix(indent)

    println("$(prefix)ConstantNode: $(string(n.value))")
end

function print_node(n::SymbolicNode, indent=0)
    prefix = generate_prefix(indent)


    if n.operation !== nothing
        println("$(prefix)SymbolicNode, operation: $(string(n.operation)), side=$(n.side)")
    else
        println("$(prefix)SymbolicNode, contents: $(n.contents_str), side=$(n.side)")
    end

    if n.left !== nothing
        print_node(n.left, indent+1)
    end
    if n.right !== nothing
        print_node(n.right, indent+1)
    end
end

function generate_prefix(indent::Int)::String
    prefix = ""
    for i = 1:indent
        prefix *= base_indent
    end
    return prefix
end
