#=
Types:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-11-04
=#

"""
Some type definitions that will be used in more than one file.
"""

"""
Options will be stored in a Dict with a String as the key.
"""
OptionType = Dict{String, Any}

"""
HeaderLookupResults - a Vector of values and field types representing the lookup of a DICOM header tag for a single file.
HeaderLookupResults has the structure [(value, field_type)]. value is the value, as the same datatype as returned by the DICOM lookup method used (i.e., without prior type conversion). field_type is a String identifying the Value Representation of the DICOM field for later conversion. If no Value Representation could be found, field_type is nothing instead.
"""
HeaderLookupResults = Vector{Tuple{Any, Union{String, Nothing}}}


"""
MultipleLookupValues - a Dict to represent multiple values for a single DICOM lookup (e.g., when the attribute has a multiplicity > 1). The DICOM keyword is stored as a String;
the values are stored as a Vector.
"""
MultipleLookupValues = Dict{String,Vector}

"""
EvaluationItem - encapsulates the result of a node evaluation.
- target: the file for which the evaluation has been performed. This might be added later, since
this information is not available inside the evaluate_* function where the object is generated.
- match: Indicates whether the evaluation performed successfully or not. For ConstantNode evaluations,
 the evaluation is always successful. For a HeaderNode evaluation, the evaluation is successful if
   the header tag exists. For ConditionNode evaluations, the value indicates the outcome of the logical operation between the two child nodes.
- node_info: This field can be used for storing some information for special purposes.
"""
mutable struct EvaluationItem
    target::String
    match::Bool
    node_info::Any
    values::MultipleLookupValues
end

# convenience constructor, where each value has to be passed explicitly:
EvaluationItem(;target::String="", match::Bool=true, node_info::Any=nothing, values::MultipleLookupValues=MultipleLookupValues()) = EvaluationItem(target, match, node_info, values)

"""
add_to_dict!(d::MultipleLookupValues, key::String, val::Any)
Add a value to d.values (a Dict of type Dict{String, Vector}. If key already exists, val is appended
to the list stored for the key. If key does not yet exist in the Dict, a new list with val as the only
element is created and stored.

As a result, d.values is changed in place.
"""
function add_to_dict!(d::MultipleLookupValues, key::String, val::Any)

    # make sure that we are adding a vector, not a single value
    add_vector = isa(val, Vector) ? val : [val]

    if key in keys(d) # key already exists -> append val to the list
        v = d[key]
        d[key] = union(v, add_vector)
    else  # key does not yet exist -> add it
        d[key] = add_vector
    end
end

"""
merge_dict!(target::MultipleLookupValues, source::MultipleLookupValues)
Merge the .values Dict's of target and source. All keys contained in source.values are copied over into
target.values, thus target.values will be changed during the process. For keys already present in
target.values, the values from source will be appended.

"""
function merge_dict!(target::MultipleLookupValues, source::MultipleLookupValues)

    for (k, v) in source
        add_to_dict!(target, k, v)
    end
end