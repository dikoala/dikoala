#=
runtests.jl:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-09-16
- This file is licensed under the 3-clause BSD license. See License.md for the full license text.
=#

using Test
using TestReports

include("../src/Types.jl")
include("../src/DicomStuff.jl")
include("../src/BinaryExpressionTree.jl")
include("../src/ConditionParser.jl")
include("../src/DirectoryScanner.jl")

using Dates
using DICOM


#=
Different test suites using different working directories, so relative paths to resource files do
not work. So we set the path explicitly
=#
this_path = dirname(Base.source_path())


# Prepare global data for the test
dcm_file = joinpath(this_path, "test_image.dcm")
dcm_data = read_dicom_header(dcm_file)

# Some helper functions
# test if f is a logical and function
is_and(f)::Bool = f(true, true) && !f(true,false) && !f(false,true) && !f(false,false)

# test if f is a logical or function
is_or(f)::Bool = f(true,true) && f(true,false) && f(false,true) && !f(false,false)

# test if f is a "contains function" (verrry cheapo version!
is_contains(f)::Bool = f("Hello", "e") && !f("Hello", "x")


@testset "DicomStuff" begin

    @testset "parse_field" begin

        @testset "Testing date parsing" begin
            a = parse_field("20211213", "DA")
            ref = Date(2021, 12, 13)
            @test a == ref

            # check bounds
            @test_throws ArgumentError parse_field("20211313", "DA")
            @test_throws ArgumentError parse_field("20210013", "DA")
            @test_throws ArgumentError parse_field("20211200", "DA")
            @test_throws ArgumentError parse_field("20211232", "DA")
        end  # @testset "Testing date parsing" begin

        @testset "Testing time parsing" begin
            a = parse_field("123109", "TM")
            cmp = Time(12, 31, 09)
            @test a == cmp

            # check bounds
            @test_throws ArgumentError parse_field("250000", "TM")
            @test_throws ArgumentError parse_field("006000", "TM")
            @test_throws ArgumentError parse_field("000061", "TM")
        end  # @testset "Testing time parsing" begin

        @testset "Testing datetime parsing" begin
            a = parse_field("20211223225913", "DT")
            ref = DateTime(2021, 12, 23, 22, 59, 13)
            @test a == ref

            # check that the optional end of the date string is ignored:
            a = parse_field("20211223225913.123456+0230", "DT")
            @test a == ref

            # check bounds
            @test_throws ArgumentError parse_field("20210023225913", "DT")
            @test_throws ArgumentError parse_field("20211323225913", "DT")
            @test_throws ArgumentError parse_field("20210100225913", "DT")
            @test_throws ArgumentError parse_field("20210132225913", "DT")
            @test_throws ArgumentError parse_field("20210101250000", "DT")
            @test_throws ArgumentError parse_field("20210101236100", "DT")
            @test_throws ArgumentError parse_field("20210101230061", "DT")
        end  # @testset "Testing datetime parsing" begin

        @testset "Value representation parsing" begin
            # Test that the different Value Representations are parsed correctly
            function test_value(keyword::String, expected_type::DataType, expected_value)::Bool

                res = header_lookup_standard(dcm_data, keyword)
                res = res[1]  # in case multiple values are returned, we use the first one
                val = parse_field(res[1], res[2])
                
                if typeof(val) != expected_type
                    println("Type mismatch for field $keyword. Expected type: $expected_type, actual type: $(typeof(val))")
                    return false
                end
                if val != expected_value
                    println("Value mismatch for $keyword. Expected value: $expected_value, actual_value: $val")
                    return false
                end
                return true
            end

            
            # AE (Application Entity)
            ## not implemented yet
            
            # Age String (AS)
            @test test_value("PatientAge", String, "066Y")

            # Code String (CS)
            @test test_value("ImageType", String, "ORIGINAL--PRIMARY--M--ND")
            
            # DA (Date)
            @test test_value("StudyDate", Date, Date(2019, 6, 6))

            # DS (Decimal String)
            @test test_value("FlipAngle", Float64, 9)

            # DT (DateTime)
            ## TODO

            # FL (Floating Point Single)
            ## TODO

            # FD (Floating Point Double)
            ## TODO

            # IS (Integer String)
            @test test_value("EchoTrainLength", Int, 1)

            # LO (Long String)
            @test test_value("Manufacturer", String, "SIEMENS")

            # LT (Long Text)
            ## TODO

            # OB (Other Byte String)
            ## not implemented yet

            # OF (Other Float String)
            ## not implemented yet

            # OW (Other WOrd String)
            ## not implemented

            # PN (Person Name)
            @test test_value("PatientName", String, "Fruity McFruitface")

            # SH (Short String)
            @test test_value("StationName", String, "Scanner McScannerface")

            # SL (Signed long)
            ## not implemented

            # SQ (Sequence of Items)
            ## not implemented

            # SS (Signed Short)
            ## not implemented

            # ST (Short text)
            @test test_value("InstitutionAddress", String, "In a country far, far away")
                        
            # TM (Time)
            @test test_value("StudyTime", Time, Time(9, 36, 30, 647))

            # UI (Unique Identifier)
            @test test_value("SOPClassUID", String, "1.2.840.10008.5.1.4.1.1.4")

            # UL (Unsigned Long)
            ## not implemented
            
            # US (Unsigned Short)
            @test test_value("Rows", UInt16, 256)

            # UT (Unlimited Text)
            ## not implemented


        end  # @testset "Value representation parsing" begin
    end  # @testset "parse_field" begin
end  # @testset ""DicomStuff"

@testset "BinaryExpressionTree" begin

    @testset "Testing helper functions" begin
        @test make_date("20210928") == Date(2021, 9, 28)
        @test make_date("2021-09-28") == Date(2021, 9, 28)
        @test_throws ErrorException make_date("210928")

        @test make_datetime("20210928 14:21:17") == DateTime(2021, 9, 28, 14, 21, 17)
        @test make_datetime("2021-09-28 14:21:17") == DateTime(2021, 9, 28, 14, 21, 17)
    end  # @testset "Testing helper functions" begin


    @testset "Testing nodes" begin


        # we will also use dcm_data as a dummy object. It will not be assessed, but a DICOMData
        # object has to be passed
        dummy_dicom = dcm_data

        @testset "Testing evaluate_node" begin
            date1 = Date(2021, 09, 14)
            date2 = Date(2021, 09, 15)

            dn1 = ConstantNode(date1)
            res = evaluate_node(dn1, dummy_dicom)
            @test res.node_info === date1
            
            dn2 = ConstantNode(date2)

            hn1 = HeaderNode("PatientName")
            res = evaluate_node(hn1, dcm_data)
            @test haskey(res.values, "PatientName") && res.values["PatientName"] == ["Fruity McFruitface"]

            hn2 = HeaderNode("StudyDate")
            res = evaluate_node(hn2, dcm_data)
            @test haskey(res.values, "StudyDate") && res.values["StudyDate"] == [Date(2019, 6, 6)]

            hn3 = HeaderNode("StudyTime")
            res = evaluate_node(hn3, dcm_data)
            @test haskey(res.values, "StudyTime") && res.values["StudyTime"] == [Time(9, 36, 30, 647)]

        end  # @testset "Testing evaluate_node" begin
    end  # @testset "Testing nodes" begin

    @testset "Testing expression parsing" begin

        cn1 = make_condition_tree("StudyDate<2021-09-28")
        res = evaluate_node(cn1, dcm_data)
        @test res.match == true

        # the same with white spaces:
        cn1a = make_condition_tree("StudyDate < 2021-09-28")
        res = evaluate_node(cn1a, dcm_data)
        @test res.match == true

        cn2 = make_condition_tree("StudyDate>2021-09-28")
        res = evaluate_node(cn2, dcm_data)
        @test res.match == false

        cn3 = make_condition_tree("StudyDate==2021-09-28")
        res = evaluate_node(cn3, dcm_data)
        @test res.match == false

        cn4 = make_condition_tree("StudyDate>=2021-09-28")
        res = evaluate_node(cn3, dcm_data)
        @test res.match == false

        cn5 = make_condition_tree("StudyDate<=2021-09-28")
        res = evaluate_node(cn5, dcm_data)
        @test res.match == true

    end  # @testset "Testing expression parsing" begin
end

@testset "ConditionParser" begin

    cond = "StudyDate"
    tree = make_condition_tree(cond)
    @test isa(tree, HeaderNode)
    @test tree.keyword == cond

    cond = "PatientName == Mr Coyote"
    tree = make_condition_tree(cond)
    @test isa(tree, ConditionNode)
    @test isa(tree.left, HeaderNode)
    @test tree.left.keyword == "PatientName"
    @test tree.op == ==  # looks weird, but we're testing whether tree.op is the == function'
    @test isa(tree.right, ConstantNode)
    @test tree.right.value == "Mr Coyote"

    cond = "(PatientName == Mr Coyote) or (StudyDate < 2018-10-21)"
    tree = make_condition_tree(cond)
    @test isa(tree, ConditionNode)
    @test tree.op(true, false) && tree.op(false, true)  && tree.op(true, true) && !tree.op(false,false)  # is there a better way to check if tree.op is the "or" function?
    @test isa(tree.left, ConditionNode)
    @test isa(tree.left.left, HeaderNode)
    @test tree.left.left.keyword == "PatientName"
    @test tree.left.op == ==  # looks weird, but we're testing whether tree.op is the == function'
    @test isa(tree.left.right, ConstantNode)
    @test tree.left.right.value == "Mr Coyote"
    @test isa(tree.right, ConditionNode)
    @test isa(tree.right.left, HeaderNode)
    @test tree.right.left.keyword == "StudyDate"
    @test tree.right.op == <
    @test isa(tree.right.right, ConstantNode)
    @test tree.right.right.value == "2018-10-21"

    @testset "Compound conditions" begin
        # Testing a condition "a and b or c". Expected structure is "a and (b or c)."
        tree = make_condition_tree("(cond1==a) and (cond2>b) or (cond3 contains c)")
        @test isa(tree, ConditionNode)
        @test is_and(tree.op)
        @test isa(tree.left, ConditionNode)
        @test tree.left.op == (==)
        @test isa(tree.left.left, HeaderNode)
        @test tree.left.left.keyword == "cond1"
        @test isa(tree.left.right, ConstantNode)
        @test tree.left.right.value == "a"
        @test isa(tree.right, ConditionNode)
        @test is_or(tree.right.op)
        @test isa(tree.right.left, ConditionNode)
        @test tree.right.left.op == (>)
        @test isa(tree.right.left.left, HeaderNode)
        @test tree.right.left.left.keyword == "cond2"
        @test isa(tree.right.left.right, ConstantNode)
        @test tree.right.left.right.value == "b"
        @test isa(tree.right.right, ConditionNode)
        @test is_contains(tree.right.right.op)
        @test isa(tree.right.right.left, HeaderNode)
        @test tree.right.right.left.keyword == "cond3"
        @test isa(tree.right.right.right, ConstantNode)
        @test tree.right.right.right.value == "c"

        # Grouping the last two conditions in the above example should not change anything. Copied all tests from above.
        tree = make_condition_tree("(cond1==a) and ((cond2>b) or (cond3 contains c))")
        @test isa(tree, ConditionNode)
        @test is_and(tree.op)
        @test isa(tree.left, ConditionNode)
        @test tree.left.op == (==)
        @test isa(tree.left.left, HeaderNode)
        @test tree.left.left.keyword == "cond1"
        @test isa(tree.left.right, ConstantNode)
        @test tree.left.right.value == "a"
        @test isa(tree.right, ConditionNode)
        @test is_or(tree.right.op)
        @test isa(tree.right.left, ConditionNode)
        @test tree.right.left.op == (>)
        @test isa(tree.right.left.left, HeaderNode)
        @test tree.right.left.left.keyword == "cond2"
        @test isa(tree.right.left.right, ConstantNode)
        @test tree.right.left.right.value == "b"
        @test isa(tree.right.right, ConditionNode)
        @test is_contains(tree.right.right.op)
        @test isa(tree.right.right.left, HeaderNode)
        @test tree.right.right.left.keyword == "cond3"
        @test isa(tree.right.right.right, ConstantNode)
        @test tree.right.right.right.value == "c"

        # Grouping the first two conditions in the above example should make a difference. Testing this hypothesis:
        tree = make_condition_tree("((cond1==a) and (cond2>b)) or (cond3 contains c)")
        @test isa(tree, ConditionNode)
        @test is_or(tree.op)
        @test isa(tree.left, ConditionNode)
        @test is_and(tree.left.op)
        @test isa(tree.left.left, ConditionNode)
        @test tree.left.left.op == (==)
        @test isa(tree.left.left.left, HeaderNode)
        @test tree.left.left.left.keyword == "cond1"
        @test isa(tree.left.left.right, ConstantNode)
        @test tree.left.left.right.value == "a"
        @test isa(tree.left.right, ConditionNode)
        @test tree.left.right.op == (>)
        @test isa(tree.left.right.left, HeaderNode)
        @test tree.left.right.left.keyword == "cond2"
        @test isa(tree.left.right.right, ConstantNode)
        @test tree.left.right.right.value == "b"
        @test isa(tree.right, ConditionNode)
        @test is_contains(tree.right.op)
        @test isa(tree.right.left, HeaderNode)
        @test tree.right.left.keyword == "cond3"
        @test isa(tree.right.right, ConstantNode)
        @test tree.right.right.value == "c"

    end  #testset Compound conditions

end  # @testset "ConditionParser"

@testset "Some cross tests" begin
    # make sure that DicomStuff.parse_field and BinaryExpressionTree.make_date/time/datetime
    # produce comparable results
    
    hn1 = HeaderNode("StudyDate")  # automatic Date generation via DicomStuff.parse_field
    dn1 = ConstantNode(make_date(dcm_data["StudyDate"]))  # Explicit Date generation
    cn1 = ConditionNode(hn1, ==, dn1, "==")
    res = evaluate_node(cn1, dcm_data)
    @test res.match == true

    hn2 = HeaderNode("StudyTime")
    dn2 = ConstantNode(make_time(String(dcm_data["StudyTime"])))
    cn2 = ConditionNode(hn2, ==, dn2, "==")
    res = evaluate_node(cn2, dcm_data)
    @test res.match == true
end

@testset "Search functionality test" begin
    
    # run some local tests, until we have some anonymized test data that can be uploaded to gitlab
    if isdir("Lumina")

        options::Dict{String,Any} = Dict(
            "path" => "Lumina"
        )


        options_mod = copy(options)
        options_mod["condition_string"] = "PatientName contains J"
        results = perform_scan(options_mod)
        @test length(results) == 289

        options_mod["reverse"] = true
        results_rev = perform_scan(options_mod)
        @test length(results_rev) == 289

        #@test results == reverse(results_rev)

        options_mod = copy(options)
        options_mod["condition_string"] = "SeriesTime > 15:00:00"
        results = perform_scan(options_mod)
        @test length(results) == 90



        
    else
        println("Could not find a folder with DICOM images for testing. Skipping tests")
    end

    
end



# test if f is a logical and function
is_and(f)::Bool = f(true, true) && !f(true,false) && !f(false,true) && !f(false,false)

# test if f is a logical or function
is_or(f)::Bool = f(true,true) && f(true,false) && f(false,true) && !f(false,false)
